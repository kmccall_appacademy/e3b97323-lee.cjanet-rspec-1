def translate(string)
  string.split.map { |word| word = pig_latin(word) }.join(" ")
end


def pig_latin(word)
  vowels = "aeiou".chars
  consonants = ('a'..'z').to_a - vowels << "qu"

  return (word+"ay") if vowels.include?(word[0])

  i=0
  while i < word.length
    break if vowels.include?(word[i])
    if consonants.include?(word[i..(i+1)])  #accounts for q/ 2 letter consonants
      i +=2
    else
      consonants.include?(word[i])      #accounts for single consonants
      i+=1
    end
  end

  word[i..word.length]+word[0..(i-1)]+"ay"
end
