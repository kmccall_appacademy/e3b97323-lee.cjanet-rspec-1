def ftoc(fahrenheit)
  (fahrenheit - 32) * (5.to_f/9)
end

def ctof(celsius)
  (celsius / (5.to_f/9)) + 32
end
