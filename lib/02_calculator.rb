def add(one, two)
 one + two
end

def subtract(one, two)
 one - two
end

def sum(array)
  array.inject(0, :+)
end

def multiply(array)
  array.inject(1,:*)
end


def power(one, two)
  one**two
end

def factorial(num)
  return 0 if num == 0

  (1..num)
    .to_a
    .reverse
    .inject(1, :*)

end
