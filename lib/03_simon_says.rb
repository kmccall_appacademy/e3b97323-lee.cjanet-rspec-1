def echo(string)
  string
end


def shout(string)
  string.upcase
end


def repeat(string, time=2)
  result_string = []

  time.times {result_string << string}

  result_string.join(" ")
end


def start_of_word(string, num)
  string[0...num]
end


def first_word(string)
  string.split[0]
end


def titleize(string)
  string.split.map.with_index do |word, idx|
    idx == 0 ? word.capitalize : capitalize(word)
  end.join(" ")
end

def capitalize(word)
  avoid = ["and", "or", "the", "over"]
  avoid.include?(word) ? word : word.capitalize
end
